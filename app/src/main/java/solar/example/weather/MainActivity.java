package solar.example.weather;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;
//terrible pollo el commit
public class MainActivity extends AppCompatActivity {

    private TextView wchillan;
    private TextView wconcepcion;
    private TextView wsantiago;
    private TextView humedadchillan;
    private TextView presionchillan;
    private TextView humedadconcepcion;
    private TextView presionconcepcion;
    private TextView humedadsantiago;
    private TextView presionsantiago;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.wchillan = (TextView) findViewById(R.id.Wchillan);
        this.wconcepcion = (TextView) findViewById(R.id.Wconcepcion);
        this.wsantiago = (TextView) findViewById(R.id.Wsantiago);
        this.humedadchillan = (TextView) findViewById(R.id.Humedadchillan);
        this.presionchillan = (TextView) findViewById(R.id.Presionchillan);
        this.humedadconcepcion = (TextView) findViewById(R.id.Humedadconcepcion);
        this.presionconcepcion = (TextView) findViewById(R.id.Presionconcepcion);
        this.humedadsantiago = (TextView) findViewById(R.id.Humedadsantiago);
        this.presionsantiago = (TextView) findViewById(R.id.Presionsantiago);


        String url = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6066399&lon=-72.1034393&appid=9e9a7a4fd59a61fa2091c106320282f5&units=metric";
        StringRequest solicitud = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String id = respuestaJSON.getString("id");

                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double temp = tempJSON.getDouble("temp");

                            Toast.makeText(getApplicationContext(), "El clima es " + temp + " Grados celcius", Toast.LENGTH_LONG).show();
                            wchillan.setText(temp + " Grados celcius");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud);

//asd
        String urlh1 = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6066399&lon=-72.1034393&appid=9e9a7a4fd59a61fa2091c106320282f5&units=metric";
        StringRequest solicitudh1 = new StringRequest(
                Request.Method.GET,
                urlh1,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String id = respuestaJSON.getString("id");

                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double pressure = tempJSON.getDouble("pressure");

                            humedadchillan.setText(pressure + " de presion");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEsperah1 = Volley.newRequestQueue(getApplicationContext());
        listaEsperah1.add(solicitudh1);



        //asd


        //asd
        String urlp1 = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6066399&lon=-72.1034393&appid=9e9a7a4fd59a61fa2091c106320282f5&units=metric";
        StringRequest solicitudp1 = new StringRequest(
                Request.Method.GET,
                urlp1,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String id = respuestaJSON.getString("id");

                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double humidity = tempJSON.getDouble("humidity");

                            presionchillan.setText(humidity + " de humedad");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEsperap1 = Volley.newRequestQueue(getApplicationContext());
        listaEsperap1.add(solicitudp1);



        //asd

        String urlh2 = "http://api.openweathermap.org/data/2.5/weather?lat=-36.8269900&lon=-73.0497700&appid=9e9a7a4fd59a61fa2091c106320282f5&units=metric";
        StringRequest solicitudh2 = new StringRequest(
                Request.Method.GET,
                urlh2,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String id = respuestaJSON.getString("id");

                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double pressure = tempJSON.getDouble("pressure");

                            humedadconcepcion.setText(pressure + " de presion");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEsperah2 = Volley.newRequestQueue(getApplicationContext());
        listaEsperah2.add(solicitudh2);


        String urlp2 = "http://api.openweathermap.org/data/2.5/weather?lat=-36.8269900&lon=-73.0497700&appid=9e9a7a4fd59a61fa2091c106320282f5&units=metric";
        StringRequest solicitudp2 = new StringRequest(
                Request.Method.GET,
                urlp2,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String id = respuestaJSON.getString("id");

                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double humidity = tempJSON.getDouble("humidity");

                            presionconcepcion.setText(humidity + " de presion");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEsperap2 = Volley.newRequestQueue(getApplicationContext());
        listaEsperap2.add(solicitudp2);


        String url2 = "http://api.openweathermap.org/data/2.5/weather?lat=-36.8269900&lon=-73.0497700&appid=9e9a7a4fd59a61fa2091c106320282f5&units=metric";
        StringRequest solicitud2 = new StringRequest(
                Request.Method.GET,
                url2,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String id = respuestaJSON.getString("id");

                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double temp = tempJSON.getDouble("temp");

                            Toast.makeText(getApplicationContext(), "El clima es " + temp + " Grados celcius", Toast.LENGTH_LONG).show();
                            wconcepcion.setText(temp + " Grados celcius");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEspera2 = Volley.newRequestQueue(getApplicationContext());
        listaEspera2.add(solicitud2);



        String url3 = "http://api.openweathermap.org/data/2.5/weather?lat=-33.4569397&lon=-70.6482697&appid=9e9a7a4fd59a61fa2091c106320282f5&units=metric";
        StringRequest solicitud3 = new StringRequest(
                Request.Method.GET,
                url3,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String id = respuestaJSON.getString("id");

                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double temp = tempJSON.getDouble("temp");

                            Toast.makeText(getApplicationContext(), "El clima es " + temp + " Grados celcius", Toast.LENGTH_LONG).show();
                            wsantiago.setText(temp + " Grados celcius");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEspera3 = Volley.newRequestQueue(getApplicationContext());
        listaEspera3.add(solicitud3);



        String urlp3 = "http://api.openweathermap.org/data/2.5/weather?lat=-33.4569397&lon=-70.6482697&appid=9e9a7a4fd59a61fa2091c106320282f5&units=metric";
        StringRequest solicitudp3 = new StringRequest(
                Request.Method.GET,
                urlp3,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String id = respuestaJSON.getString("id");

                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double humidity = tempJSON.getDouble("humidity");

                            presionsantiago.setText(humidity + " de humedad");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEsperap3 = Volley.newRequestQueue(getApplicationContext());
        listaEsperap3.add(solicitudp3);



        String urlh3 = "http://api.openweathermap.org/data/2.5/weather?lat=-33.4569397&lon=-70.6482697&appid=9e9a7a4fd59a61fa2091c106320282f5&units=metric";
        StringRequest solicitudh3 = new StringRequest(
                Request.Method.GET,
                urlh3,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            String id = respuestaJSON.getString("id");

                            JSONObject tempJSON = respuestaJSON.getJSONObject("main");
                            double pressure = tempJSON.getDouble("pressure");

                            humedadsantiago.setText(pressure + " de presion");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        RequestQueue listaEsperah3 = Volley.newRequestQueue(getApplicationContext());
        listaEsperah3.add(solicitudh3);



    }
}
